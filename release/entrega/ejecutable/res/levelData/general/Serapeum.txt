{"width": 48,

"height":64,

"entities":[
{"name": "Darwishi",
 "type": "dynamic",
 "posX": 24,
 "posY": 30 },
{"name": "NPCSerapeum1",
 "type": "dynamic",
 "posX": 18,
 "posY": 41 },
{"name": "NPCSerapeum2",
 "type": "dynamic",
 "posX": 28,
 "posY": 22 }
],

"animations":[
    {"index":0,
    "path": "Serapeum\/background",
    "numFrames": 1
    }, {"index":63,
    "path": "Serapeum\/foreground",
    "numFrames": 1
    }
],

"sound" : "templo"
}