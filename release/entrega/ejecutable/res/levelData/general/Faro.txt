{"width": 128,

"height":32,

"entities":[
 {"name": "BlockingFaro",
   "type": "static",
  "posX": 20,
  "posY": 12},

{"name": "BlockingFaro",
   "type": "static",
  "posX": 20,
  "posY": 13},

{"name": "BlockingFaro",
   "type": "static",
  "posX": 20,
  "posY": 14}
],

"animations":[
    {"index":0,
    "path": "Faro\/background",
    "numFrames": 1
    },
    
    {"index":31,
    "path": "Faro\/foreground",
    "numFrames": 1
    }
],

"sound" : "bb"
}