{"width": 64,

"height":64,

"entities":[
 {"name": "Akil",
   "type": "dynamic",
   "posX": 28,
   "posY": 29},
],

"animations":[
    {"index":0,
    "path": "UpstairsFaro\/background",
    "numFrames": 1
    },
    
    {"index":63,
    "path": "UpstairsFaro\/foreground",
    "numFrames": 1
    }
],

"sound" : "bb"
}