import java.io.File;

import presentacion.render.View;
public class Main {

	public static void main(String[] args) {
		//Para la retina? Yes
		//-Dorg.lwjgl.opengl.Display.enableHighDPI=false
		System.setProperty("org.lwjgl.opengl.Display.enableHighDPI", "false");
		//Para los jar
		System.setProperty("org.lwjgl.librarypath", new File("lib/natives").getAbsolutePath());
		View view = new View();
		view.init();
	}
}
