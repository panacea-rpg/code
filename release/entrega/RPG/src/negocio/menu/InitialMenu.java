package negocio.menu;

import negocio.logic.BusinessLayer;
import negocio.logic.LogicInput;

public abstract class InitialMenu{
	private int numOptions;
	protected int selectedOption;
	protected int selectedConfig;
	private String ID;
	protected BusinessLayer bl;
	public InitialMenu(int numOptions, String ID,BusinessLayer bl) {
		this.numOptions = numOptions;
		this.selectedOption = 0;
		this.selectedConfig = 0;
		this.ID = ID;
		this.bl = bl;
	}
	
	public void takeOrders(){ 

		if(LogicInput.ACTION.isDown()){
			this.action();
    		LogicInput.Menulock();
    	}
    	
    	else if (LogicInput.EXIT.isDown()) {
    		this.exit();
    		LogicInput.Menulock();
    	}
    	
    	else if(LogicInput.UP.isDown()) {
    		if(selectedOption > 0)--selectedOption;
    		LogicInput.Menulock();
    	}
    	           
    	else if(LogicInput.DOWN.isDown()) {
    		if(selectedOption < numOptions - 1)++selectedOption;
    		LogicInput.Menulock();
    	}
    	else if(LogicInput.RIGHT.isDown()) {
    		if(selectedConfig < numOptions - 1)++selectedConfig;
    		LogicInput.Menulock();
    	}
    	           
    	else if(LogicInput.LEFT.isDown()) {
    		if(selectedConfig > 0)--selectedConfig;
    		LogicInput.Menulock();
    	}	
    }
	public abstract void action();
	public abstract void exit();
	public String getID() {
		return ID;
	}
	public int getSelected() {
		return this.selectedOption;
	}


}
