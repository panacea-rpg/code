package negocio.GameMenus;

import negocio.logic.Game;
import negocio.logic.Inventory;
import negocio.logic.Item;
import negocio.logic.LogicInput;
import negocio.transfer.ItemTransfer;

public class InventoryMenu extends Menu {
	private Item[] matrix;
	private int x;
	private DialogMenu menu;

	public InventoryMenu(Game game, Inventory inventory) {
		super(game);
		this.x = 0;
		this.matrix = inventory.getItems();
	}

	public void takeOrders() {
		if (menu != null) {
			boolean cerrado = menu.takeInventoryOrders(matrix[x].getEvent());
			if (cerrado) {
				menu = null;
				game.onDialogDestroyed();
			}
		} else {
			if (LogicInput.EXIT.isDown() || LogicInput.BACK.isDown() || LogicInput.INVENTORY.isDown() ){
				game.setMenu(null);
				game.onInventoryDestroyed();
				LogicInput.Menulock();
			} else if (LogicInput.UP.isDown()) {
				--x;
				LogicInput.Menulock();
			} else if (LogicInput.DOWN.isDown()) {
				++x;
				LogicInput.Menulock();
			} else if (LogicInput.ACTION.isDown() && matrix.length != 0 && matrix[x].getDialog() != null ) {
				menu = new DialogMenu(null, matrix[x].getDialog(), game);
				game.onDialogCreated();
				LogicInput.Menulock();
			}
			if (matrix.length != 0)
				x = x % matrix.length;
			else x = 0;
		}
	}

	public ItemTransfer getInventoryItems() {
		String l[] = new String[matrix.length];
		for (int i = 0; i < matrix.length; ++i) {
			if (matrix[i] == null)
				l[i] = null;
			else
				l[i] = matrix[i].getId();
		}
		ItemTransfer t = new ItemTransfer();
		t.setItems(l);
		t.setSelectedOption(x);
		return t;
	}

	public String getDialogMenuTransfer() {
		if (menu != null)
			return menu.getDialogMenuTransfer();
		else
			return null;
	}

	public void refresh() {
	}
}
