package negocio.event;

import negocio.logic.Game;
import negocio.logic.Item;

public abstract class ItemEvent extends Event {
    protected final Item item;
	
	public ItemEvent(String id, Item item, Game game) {
		super(id, game);
		this.item = item;
	}

}
