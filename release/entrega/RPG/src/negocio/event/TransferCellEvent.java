package negocio.event;

public class TransferCellEvent {
    private String level;
    private String name;
    private int x;
    private int y;
    private String destLevel;
    private int destX;
    private int destY;
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getDestLevel() {
		return destLevel;
	}
	public void setDestLevel(String destLevel) {
		this.destLevel = destLevel;
	}
	public int getDestX() {
		return destX;
	}
	public void setDestX(int destX) {
		this.destX = destX;
	}
	public int getDestY() {
		return destY;
	}
	public void setDestY(int destY) {
		this.destY = destY;
	}
    
    
}
