package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class CrazyModeOn extends EntityEvent {
	public CrazyModeOn(Entity entity, Game game) {
		super("CrazyModeOn" , entity, game);
		
	}

	public void execute() {
		game.setCrazyMode(true);
		game.setEntityState("Captain", 3);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		game.getPlayer().setAct(true);
	}
}
