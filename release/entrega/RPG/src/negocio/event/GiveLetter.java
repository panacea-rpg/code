package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class GiveLetter extends EntityEvent {

	public GiveLetter(Entity entity, Game game) {
		super("GiveLetter" , entity, game);
	}

	public void execute() {
		game.setEntityState(entity.getId(), game.getEntityState(entity.getId())+1);
		game.addToInventory("letter");
	}

}
