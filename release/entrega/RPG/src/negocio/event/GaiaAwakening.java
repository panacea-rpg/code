package negocio.event;

import negocio.action.Act;
import negocio.action.Turn;
import negocio.logic.Facing;
import negocio.logic.Game;

public class GaiaAwakening extends CellEvent {
   public GaiaAwakening(int x, int y, Game game) {
	   super("GaiaAwakening", x, y, game);
   }
   
   public void execute() {
	   if(game.getEntityState("Adio") == 0) {
	    game.getPlayer().addOrder(new Turn(game.getPlayer(),Facing.BACK));
	    game.getPlayer().addOrder(new Turn(game.getPlayer(),Facing.FRONT));
	    game.getPlayer().addOrder( new Turn(game.getPlayer(),Facing.RIGHT));
		game.getPlayer().addOrder(new Act(game.getPlayer()));
		game.getCell(x, y).setEvent(null);
		game.setEntityState("Adio", 1);
	   }
   }
}
