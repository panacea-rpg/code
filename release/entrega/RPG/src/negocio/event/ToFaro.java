package negocio.event;

import negocio.entities.Entity;
import negocio.logic.Game;

public class ToFaro extends EntityEvent {
   public ToFaro(Entity entity, Game game) {
	   super("ToFaro",entity, game);
   }
   
   public void execute() {
	   game.setEntityState("BlockingFaro", -1);
	   game.setEntityState(entity.getId(), 3);
   }
}
