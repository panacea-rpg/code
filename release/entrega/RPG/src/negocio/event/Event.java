package negocio.event;

import negocio.logic.Game;

public abstract class Event {
	protected final String id;
    protected final Game game;
	
	public Event(String id,Game game) {
		this.id = id;
		this.game = game;
	}
	
	public String getId() {
		return id;
	}
	
	public abstract void execute();

}
