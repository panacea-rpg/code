package negocio.transfer;

public class EntityUpdateTransfer {
	private String id; 
	private String type; // dynamic o static.Es un parametro de entrada al DAO e indica si tiene que cargar lo siguiente o no:
	private int state; // el estado
	
	private DialogTransfer dialog;
	private String event; 
	private int facing;
	
	//solo para dynamic
	private	boolean stop;  
	private int maxX;
	private int maxY;
	private int minX;
	private int minY;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	
	public DialogTransfer getDialog() {
		return dialog;
	}
	public void setDialog(DialogTransfer dialog) {
		this.dialog = dialog;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public int getFacing() {
		return facing;
	}
	public void setFacing(int facing) {
		this.facing = facing;
	}
	
	public boolean getStop() {
		return stop;
	}
	public void setStop(boolean stop) {
		this.stop = stop;
	}
	public int getMaxX() {
		return maxX;
	}
	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}
	public int getMaxY() {
		return maxY;
	}
	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}
	public int getMinX() {
		return minX;
	}
	public void setMinX(int minX) {
		this.minX = minX;
	}
	public int getMinY() {
		return minY;
	}
	public void setMinY(int minY) {
		this.minY = minY;
	}
	
}
