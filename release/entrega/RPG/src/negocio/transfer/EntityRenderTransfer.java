package negocio.transfer;

import negocio.logic.Facing;

public class EntityRenderTransfer {
	private String id;
	private int x,y;
	private float offset;
	private int animState;
	private int shiftX,shiftY,ind;

	public EntityRenderTransfer(int x,int y,float offset,Facing facing,String id,int animState) {
		this.x = x;
		this.y = y;
		this.shiftX = facing.getShiftx();
		this.shiftY = facing.getShifty();
		this.ind = facing.getInd();
		this.offset = offset;
		this.id = id;
		this.animState = animState;
	}
	public int getShiftX() {
		return shiftX;
	}
	public int getShiftY() {
		return shiftY;
	}
	public int getInd() {
		return ind;
	}
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public float getOffset() {
		return offset;
	}
	public String getId() {
		return id;
	}
	public int getAnimState() {
		return animState;
	}
}
