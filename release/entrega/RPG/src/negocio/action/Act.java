package negocio.action;

import negocio.entities.DynamicEntity;

public class Act extends Action {
    public Act(DynamicEntity entity) {
    	super(entity);
    }
    
    public void execute() {
    	entity.setAct(true);
    }
}
