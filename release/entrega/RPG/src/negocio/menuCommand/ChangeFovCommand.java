package negocio.menuCommand;

import negocio.logic.BusinessLayer;

public class ChangeFovCommand extends Command {
	private int levelFov;

	public ChangeFovCommand(BusinessLayer bl,int levelFov) {
		super(bl);
		this.levelFov = levelFov;
	}

	@Override
	public void execute() {
		bl.setFOV(levelFov);
	}

}
