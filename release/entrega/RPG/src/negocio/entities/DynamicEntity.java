package negocio.entities;

import negocio.action.Action;
import negocio.logic.Game;

public abstract class DynamicEntity extends Entity{
	protected boolean moving;
	protected boolean stop; 
	
	
	private final int numFrames = 4;
	protected final float speed = 2f*10f/Game.FPS;
	
     public DynamicEntity(String id, int x, int y, Game game){
    	 super(id,x,y,game);
    	 this.moving = false;
    	 this.talking = false;
     }
     
     public void setStop(boolean stop){
 		this.stop = stop;
 	}
     
     public void addOrder(Action act) {
 		orders.add(act);
 	}
 	
     public boolean advance(){
    	
    	 if(game.isPassable(x+facing.getShiftx(),y+facing.getShifty()) && 
				   !game.isOccupied(x+facing.getShiftx(),y+facing.getShifty())){
			   moving = true;
			   this.offset = -4f;
			   game.move(x, y, x+facing.getShiftx(), y+facing.getShifty());
			   x += facing.getShiftx();
  		       y += facing.getShifty();
  		       game.triggerEvent(x, y);
  		       return true;
         }
    	 else return false;
     }
     
     public void refreshAnimation(){
    	 if(moving){
    		 offset += speed;
    		 if(-2.5 < offset && offset < speed -2.5) ++animState;
    		 if(-0.5 < offset && offset < speed -0.5) ++animState;
    		 animState = animState % numFrames;
  
    		 if(offset >=0){
    			 offset = 0;
    			 moving = false;
    		 }
    	 }
     }
}
