package negocio.entities;

import negocio.GameMenus.DialogMenu;
import negocio.factories.EventFactory;
import negocio.logic.Dialog;
import negocio.logic.Facing;
import negocio.logic.Game;
import negocio.transfer.EntityUpdateTransfer;

public class NPC extends DynamicEntity {
	protected int minX;
	protected int maxX;
	protected int minY;
	protected int maxY;

	public NPC(String id, int x, int y, Game game) {
		super(id, x, y, game);
	}

	public void interact() {
		if (!moving) {
			
			prevFacing = facing;
			facing = game.getPlayer().facing.not();
			if (dialog != null) {
				game.setMenu(new DialogMenu(this, dialog, game));
				talking = true;
				game.onDialogCreated();
			}
		}
	}

	public void refresh() {
		if (orders.isEmpty()) {
			update();

			if (!stop  && !talking && game.getRand().nextDouble() < 0.005 && !moving) {
				Facing aux = Facing.values()[game.getRand().nextInt(Facing.values().length)];
				if (minX <= x + aux.getShiftx() && x + aux.getShiftx() <= maxX && minY <= y + aux.getShifty()
						&& y + aux.getShifty() <= maxX && game.isPassable(x + aux.getShiftx(), y + aux.getShifty())
						&& !game.isOccupied(x + facing.getShiftx(), y + facing.getShifty())) {
					facing = aux;
					advance();
				}
			}
		}

		else {
			orders.poll().execute();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		refreshAnimation();

	}

	public void update() {
		if (logicState != game.getEntityState(id)) {
			logicState = game.getEntityState(id);
			EntityUpdateTransfer t = game.getEntityUpdate(id, "dynamic", logicState);
			dialog = new Dialog(t.getDialog());
			event = EventFactory.createEntityEvent(t.getEvent(), this, game);
			facing = Facing.values()[t.getFacing()];
			stop = t.getStop();
			maxX = t.getMaxX();
			maxY = t.getMaxY();
			minX = t.getMinX();
			minY = t.getMinY();
		}
	}

	public int getLogic() {
		return this.logicState;
	}

}
