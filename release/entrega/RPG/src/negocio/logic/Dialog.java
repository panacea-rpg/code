package negocio.logic;

import java.util.List;

import negocio.transfer.DialogTransfer;

public class Dialog {
	private List<String> conversation;
	
	public Dialog(DialogTransfer t) {
		conversation = t.getConversation();
	}

	public List<String> getConversation() {
		return conversation;
	}
	public void setConversation(List<String> conversation) {
		this.conversation = conversation;
	}
}
