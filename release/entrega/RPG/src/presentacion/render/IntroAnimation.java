package presentacion.render;

import org.lwjgl.opengl.Display;


public class IntroAnimation extends Animation {
	private View render;

	public IntroAnimation(View render) {
		super("animations","intro","keyboard");
		this.numFrames = 1;
		this.render = render;
		AnimationTransfer[] anims = { this};
		render.loadAnimations(anims);
	}

	public void play(){
		render.setScalingToFillScreen(frames[0].getImageHeight(), frames[0].getImageWidth());
		render.renderMap(frames[0]);
		Display.update();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
