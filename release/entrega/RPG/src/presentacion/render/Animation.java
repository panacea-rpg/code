package presentacion.render;

import org.newdawn.slick.opengl.Texture;
//https://www.oracle.com/technetwork/java/transferobject-139757.html
public class Animation extends AnimationTransfer {
    private int actualFrame;

	public Animation(AnimationTransfer transfer) {
		super(transfer.getPath(),transfer.getNumFrames());
		this.frames = transfer.getFrames();
    	this.actualFrame = 0;
	}
    public Animation(String path, int numFrames){
    	super(path,numFrames);
    }
    public Animation(String path) {
		super(path);
	}
    public Animation(String p1,String p2,String p3) {
		super(p1 + "/" + p2 + "/"  + p3);
	}
    
	public Texture getFrame() {
    	return frames[actualFrame];
    }
    public Texture getFrame(int ind) {
    	return frames[ind];
    }
    public void next() {
    	actualFrame = (actualFrame + 1) % this.numFrames;
    }
    public int getImageWidth() {
    	 return frames[0].getImageWidth();
    }
    public int getImageHeight() {
    	return frames[0].getImageHeight();
    }
    
	public void play() throws InterruptedException{};
}
