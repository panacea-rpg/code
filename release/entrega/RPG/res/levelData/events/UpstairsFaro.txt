{"events":[
{"name": "Transition",
   "level": "UpstairsFaro",
   "destLevel": "Faro",
   "x": 38,
   "y": 34,
   "destX": 20,
   "destY": 12
  },
  {"name": "Transition",
   "level": "UpstairsFaro",
   "destLevel": "Faro",
   "x": 38,
   "y": 35,
   "destX": 20,
   "destY": 13
  },
  {"name": "Transition",
   "level": "UpstairsFaro",
   "destLevel": "Faro",
   "x": 25,
   "y": 35,
   "destX": 20,
   "destY": 13
  },{"name": "Transition",
   "level": "UpstairsFaro",
   "destLevel": "DownstairsFaro",
   "x": 25,
   "y": 35,
   "destX": 17,
   "destY": 37
  },
]
}
