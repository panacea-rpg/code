{"events":[
{"name": "Transition",
   "level": "Bridge",
   "destLevel": "Square",
   "x": 15,
   "y": 22,
   "destX": 63,
   "destY": 95
  },

{"name": "Transition",
   "level": "Bridge",
   "destLevel": "Square",
   "x": 16,
   "y": 22,
   "destX": 64,
   "destY": 95
  },

{"name": "Transition",
   "level": "Bridge",
   "destLevel": "Island",
   "x": 14,
   "y": 114,
   "destX": 62,
   "destY": 17
  },

{"name": "Transition",
   "level": "Bridge",
   "destLevel": "Island",
   "x": 15,
   "y": 114,
   "destX": 63,
   "destY": 17
  },

{"name": "Transition",
   "level": "Bridge",
   "destLevel": "Island",
   "x": 16,
   "y": 114,
   "destX": 64,
   "destY": 17
  },

{"name": "Transition",
   "level": "Bridge",
   "destLevel": "Island",
   "x": 17,
   "y": 114,
   "destX": 65,
   "destY": 17
  },
]
}
