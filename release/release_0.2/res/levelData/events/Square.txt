{"events":[

{"name": "Transition",
   "level": "Square",
   "destLevel": "port",
   "x": 13,
   "y": 82,
   "destX": 113,
   "destY": 53
  },{"name": "Transition",
   "level": "Square",
   "destLevel": "port",
   "x": 13,
   "y": 83,
   "destX": 113,
   "destY": 54
  },{"name": "Transition",
   "level": "Square",
   "destLevel": "port",
   "x": 13,
   "y": 84,
   "destX": 113,
   "destY": 55
  },{"name": "Transition",
   "level": "Square",
   "destLevel": "port",
   "x": 13,
   "y": 85,
   "destX": 113,
   "destY": 56
  },

{"name": "Transition",
   "level": "Square",
   "destLevel": "Bridge",
   "x": 63,
   "y": 96,
   "destX": 15,
   "destY": 23
  },

{"name": "Transition",
   "level": "Square",
   "destLevel": "Bridge",
   "x": 64,
   "y": 96,
   "destX": 16,
   "destY": 23
  },{"name": "Transition",
   "level": "Square",
   "destLevel": "Serapeum",
   "x": 63,
   "y": 48,
   "destX": 24,
   "destY": 48
  },{"name": "Transition",
   "level": "Square",
   "destLevel": "Serapeum",
   "x": 64,
   "y": 48,
   "destX": 24,
   "destY": 48
  },
]
}