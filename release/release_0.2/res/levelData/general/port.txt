{"width": 129,

"height":64,

"entities":[
 {"name": "Adio",
  "type": "dynamic",
  "posX": 54,
  "posY": 32
  }
 ],

"animations":[
    {"index":0,
    "path": "port\/background",
    "numFrames": 1
    },
    
    {"index":60,
    "path": "port\/foreground",
    "numFrames": 1
    }
],

"sound" : "bb"
}