{"sequence": [
"CAPITÁN: ¿Señorita?",
"GAIA: ¡Es el dolor de cabeza otra vez!",
"CAPITÁN: ¿Qué le ocurre, señorita? ¿Está bien?",
"GAIA: Mi...ca...be...za...due...le...mu...cho.",
"GAIA: ¡Aléjate de mi vista! ¡Fuera!",
"CAPITÁN: ¿Señorita?",
"GAIA: Es...la...enfer...me...dad...ne...ce...si...to.",
"GAIA: ¡No necesito nada! ¡Fuera!",
"GAIA: Ne...ce...si...to...puri...fi...car...me.",
"CAPITÁN: ¿Señorita? ¿Purificarse?",
"CAPITÁN: En el Serapeum, el templo, también podrá purificarse.",
"GAIA: ¡No! ¡No necesito nada de ti!"]}
