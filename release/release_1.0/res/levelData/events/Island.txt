{"events":[
{"name": "Transition",
   "level": "Island",
   "destLevel": "Bridge",
   "x": 62,
   "y": 16,
   "destX": 14,
   "destY": 113
  },

{"name": "Transition",
   "level": "Island",
   "destLevel": "Bridge",
   "x": 63,
   "y": 16,
   "destX": 15,
   "destY": 113
  },

{"name": "Transition",
   "level": "Island",
   "destLevel": "Bridge",
   "x": 64,
   "y": 16,
   "destX": 16,
   "destY": 113
  },

{"name": "Transition",
   "level": "Island",
   "destLevel": "Bridge",
   "x": 65,
   "y": 16,
   "destX": 17,
   "destY": 113
  },{"name": "Transition",
   "level": "Island",
   "destLevel": "Faro",
   "x": 13,
   "y": 62,
   "destX": 113,
   "destY": 12
  },
  {"name": "Transition",
   "level": "Island",
   "destLevel": "Faro",
   "x": 13,
   "y": 63,
   "destX": 113,
   "destY": 13
  },
  {"name": "Transition",
   "level": "Island",
   "destLevel": "Faro",
   "x": 13,
   "y": 64,
   "destX": 113,
   "destY": 14
  },{"name": "Transition",
   "level": "Island",
   "destLevel": "AbuDownstairs",
   "destX": 15,
   "destY": 18,
   "x": 90,
   "y": 50
  },{"name": "Transition",
   "level": "Island",
   "destLevel": "AbuDownstairs",
   "destX": 16,
   "destY": 18,
   "x": 91,
   "y": 50
  },
]
}