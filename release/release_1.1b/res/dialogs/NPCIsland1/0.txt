{"sequence": ["HABITANTE: Bienvenida a nuestra pequeña isla.",
"HABITANTE: Aunque estamos un poco lejos de la ciudad, sigues estando en Alejandría.",
"HABITANTE: La única difencia es que la mar arbolada se nota mucho más en esta isla.",
"HABITANTE: Al otro lado de la isla se encuentra el faro de Alejandría",
"HABITANTE: Pero no hay nada que ver allí. El farero no es más que un egipcio medio loco."]}
