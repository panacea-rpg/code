{"width": 128,

"height":128,

"entities":[
  
{"name": "NPCSquare1",
  "type": "dynamic",
  "posX": 29,
  "posY": 81},
  
{"name": "NPCSquare2",
  "type": "dynamic",
  "posX": 63,
  "posY": 65},
  
 {"name": "Fountain",
  "type": "static",
  "posX": 64,
  "posY": 58},

  {"name": "Fountain",
  "type": "static",
  "posX": 65,
  "posY": 58},

{"name": "Fountain",
  "type": "static",
  "posX": 67,
  "posY": 60},

  {"name": "Fountain",
  "type": "static",
  "posX": 67,
  "posY": 61},

{"name": "Fountain",
  "type": "static",
  "posX": 65,
  "posY": 63},

{"name": "Fountain",
  "type": "static",
  "posX": 64,
  "posY": 63},

{"name": "Fountain",
  "type": "static",
  "posX": 63,
  "posY": 63},

{"name": "Fountain",
  "type": "static",
  "posX": 62,
  "posY": 63},

{"name": "Fountain",
  "type": "static",
  "posX": 60,
  "posY": 61},

{"name": "Fountain",
  "type": "static",
  "posX": 60,
  "posY": 60},

{"name": "Fountain",
  "type": "static",
  "posX": 62,
  "posY": 58},

{"name": "Fountain",
  "type": "static",
  "posX": 63,
  "posY": 58},
],



"animations":[
    {"index":0,
    "path": "Square\/background",
    "numFrames": 1
    },
    
    {"index":127,
    "path": "Square\/foreground",
    "numFrames": 1
    }
],

"sound" : "plaza"
}