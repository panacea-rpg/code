{"sequence": ["GAIA: Hol...",
"SACERDOTE: ¿Quién eres? ¡Fuera! ¡Demasiado sucia!",
"SACERDOTE: Los dioses no quieren suciedad en un lugar sagrado.",
"GAIA: Solo quiero...",
"SACERDOTE: Pues ven a verme cuando te hayas limpiado.",
"GAIA: Para eso necesitaba su ayud...",
"SACERDOTE: ¿No me has entendido? Ve a limpiarte. A la fuente.",
"GAIA: ¿La de la plaza?",
"SACERDOTE: Sí. Pero no te bañes entera. Los dioses sabrán qué más impurezas tienes."]}
