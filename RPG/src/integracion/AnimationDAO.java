 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package integracion;

import static org.lwjgl.opengl.GL11.GL_LINEAR;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import presentacion.render.AnimationTransfer;
import presentacion.render.View;

public class AnimationDAO implements DAO_ReadOnly<AnimationTransfer> {

	@Override
	public void read(AnimationTransfer t) {
		int numFrames = t.getNumFrames();
		Texture[] frames = new Texture[numFrames];  
		
		
		for(int i = 0; i< numFrames; ++i){
			Texture texture = null;
			try {
				texture = TextureLoader.getTexture("PNG", View.class.getClassLoader().
						getResourceAsStream(t.getPath()+Integer.toString(i+1)+".png"),GL_LINEAR);
			} catch (Exception e) {
				System.out.println("Exception in Texture: " + t.getPath()+(i+1)+".png" );
			}
			
    		frames[i]= texture;
    	}
		t.setFrames(frames);
	}

}
