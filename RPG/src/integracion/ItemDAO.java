 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package integracion;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import negocio.transfer.DialogTransfer;
import negocio.transfer.TransferItem;

public class ItemDAO implements DAO_ReadOnly<TransferItem> {
    private static final String iniPath = "res/items";

	@Override
	public void read(TransferItem t) {
		if(t.getId() == null) throw new IllegalArgumentException("To load an item you must supply its id");
		
		JSONObject jo = null;

		try {
			jo = new JSONObject(new JSONTokener(new FileReader(iniPath + "/" + t.getId() + ".txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		t.setEvent(jo.getString("event"));
		try {
			DialogDAO dao = new DialogDAO();
			DialogTransfer dt = new DialogTransfer();
			dt.setId(t.getId());//el id del objeto
			dt.setLogicState(0);//state 0 para todo item
			dao.read(dt);
			t.setDialog(dt);
		} catch (Exception e) {
			System.out.println(
					"Failed loading the associated dialog for the item " + t.getId());
		}
	}

}
