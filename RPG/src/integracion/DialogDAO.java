 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package integracion;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import negocio.transfer.DialogTransfer;

public class DialogDAO implements DAO_ReadOnly<DialogTransfer> {

	@Override
	public void read(DialogTransfer t) {

		JSONObject jo = null;
		try {
			InputStreamReader is  = new InputStreamReader(new FileInputStream("res/dialogs/" + t.getId() + "/" + t.getLogicState() + ".txt"), "UTF-8");
			//FileReader fr = new FileReader("res/dialogs/" + t.getId() + "/" + t.getLogicState() + ".txt");
			jo = new JSONObject(
					new JSONTokener(is));
			//fr.close();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Cargar la secuencia de mensajes del dialogo
		try {
			JSONArray ja = jo.getJSONArray("sequence");
			List<String> sequence = new ArrayList<String>();

			for (int i = 0; i < ja.length(); ++i) {
				sequence.add(ja.getString(i));
			}
			
			t.setConversation(sequence);
		} catch (JSONException e) {
			System.out.println(
					"Failed loading message sequence for dialog " + t.getId() + " at logicState " + t.getLogicState());
		}

	}

}
