 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package integracion;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import negocio.transfer.GameTransfer;
import negocio.transfer.TransferItem;

public class GameDAO implements DAO_Updatable<GameTransfer> {
	private static final String iniPath = "res/gameData";

	@Override
	public void read(GameTransfer t) {// necesito tener el campo id cargado
		readPlayerData(t);
		readEntitiesStates(t);
		readItems(t);
	}

	private void readPlayerData(GameTransfer t) {
		JSONObject jo = null;

		try {
			jo = new JSONObject(new JSONTokener(new FileReader(iniPath + "/" + t.getId() + "/player.txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			t.setLevelName(jo.getString("levelName"));
			t.setPosX(jo.getInt("posX"));
			t.setPosY(jo.getInt("posY"));
			t.setCrazy(jo.getBoolean("crazy"));
		} catch (JSONException e) {
			System.out.println("Failed loading player's data for game: " + t.getId());
		}
	}

	private void readEntitiesStates(GameTransfer t) {
		JSONObject jo = null;

		try {
			jo = new JSONObject(new JSONTokener(new FileReader(iniPath + "/" + t.getId() + "/entities.txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			JSONArray states = jo.getJSONArray("states");
			Map<String, Integer> entitiesStates = new HashMap<String, Integer>();

			for (int i = 0; i < states.length(); ++i) {
				JSONObject act = states.getJSONObject(i);
				entitiesStates.put(act.getString("id"), act.getInt("state"));
			}

			t.setEntitiesStates(entitiesStates);
		} catch (Exception e) {
			System.out.println("Failed loading entities' states for game: " + t.getId());
		}

	}

	private void readItems(GameTransfer t) {
		JSONObject jo = null;

		try {
			jo = new JSONObject(new JSONTokener(new FileReader(iniPath + "/" + t.getId() + "/items.txt")));
		} catch (JSONException | FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			JSONArray ja = jo.getJSONArray("itemsList");
			List<TransferItem> itemsList = new ArrayList<TransferItem>();

			for (int i = 0; i < ja.length(); ++i) {
				JSONObject act = ja.getJSONObject(i);

				ItemDAO dao = new ItemDAO();
				TransferItem ti = new TransferItem();
				try {
					ti.setId(act.getString("id"));
					dao.read(ti);

					itemsList.add(ti);				
				} catch (Exception e) {
					System.out.println("Failed loading item " + act.getString("id") + " at game "
							+ t.getId());
				}

			}

			t.setItems(itemsList);
		} catch (Exception e) {
			System.out.println("Failed loading inventory for game: " + t.getId());
		}

	}

	@Override
	public void update(GameTransfer t) {
		updatePlayerData(t);
		updateEntitiesStates(t);
		updateItems(t);
	}

	private void updatePlayerData(GameTransfer t) {
		JSONObject jo = new JSONObject();

		jo.put("levelName", t.getLevelName());
		jo.put("posX", t.getPosX());
		jo.put("posY", t.getPosY());
		jo.put("crazy", t.getCrazy());

		try (FileWriter file = new FileWriter(iniPath + "/" + t.getId() + "/player.txt")) {
			file.write(jo.toString());
		} catch (IOException e) {
			System.out.println("Failed trying to save player's data for game: " + t.getId());
			e.printStackTrace();
		}
	}

	private void updateEntitiesStates(GameTransfer t) {
		JSONArray ja = new JSONArray();

		Map<String, Integer> entitiesStates = t.getEntitiesStates();
		for (Map.Entry<String, Integer> entry : entitiesStates.entrySet()) {
			JSONObject aux = new JSONObject();
			aux.put("id", entry.getKey());
			aux.put("state", entry.getValue());
			ja.put(aux);
		}

		JSONObject info = new JSONObject();
		info.put("states", ja);

		try (FileWriter file = new FileWriter(iniPath + "/" + t.getId() + "/entities.txt")) {
			file.write(info.toString());
		} catch (IOException e) {
			System.out.println("Failed trying to save entities' states for game: " + t.getId());
			e.printStackTrace();
		}
	}

	private void updateItems(GameTransfer t) {
		JSONObject info = new JSONObject();
		JSONArray ja = new JSONArray();

		List<TransferItem> itemsList = t.getItems();
		for (ListIterator<TransferItem> it = itemsList.listIterator(); it.hasNext();) {
			TransferItem act = it.next();

			JSONObject aux = new JSONObject();
			aux.put("id", act.getId());
			aux.put("event", act.getEvent());

			ja.put(aux);
		}

		info.put("itemsList", ja);

		try (FileWriter file = new FileWriter(iniPath + "/" + t.getId() + "/items.txt")) {
			file.write(info.toString());
		} catch (IOException e) {
			System.out.println("Failed trying to save inventory for game: " + t.getId());
			e.printStackTrace();
		}
	}

}
