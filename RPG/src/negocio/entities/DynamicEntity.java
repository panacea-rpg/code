 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.entities;

import negocio.action.Action;
import negocio.logic.Game;

public abstract class DynamicEntity extends Entity{
	protected boolean moving;
	protected boolean stop; 
	
	
	private final int numFrames = 4;
	protected final float speed = 2f*10f/Game.FPS;
	
     public DynamicEntity(String id, int x, int y, Game game){
    	 super(id,x,y,game);
    	 this.moving = false;
    	 this.talking = false;
     }
     
     public void setStop(boolean stop){
 		this.stop = stop;
 	}
     
     public void addOrder(Action act) {
 		orders.add(act);
 	}
 	
     public boolean advance(){
    	
    	 if(game.isPassable(x+facing.getShiftx(),y+facing.getShifty()) && 
				   !game.isOccupied(x+facing.getShiftx(),y+facing.getShifty())){
			   moving = true;
			   this.offset = -4f;
			   game.move(x, y, x+facing.getShiftx(), y+facing.getShifty());
			   x += facing.getShiftx();
  		       y += facing.getShifty();
  		       game.triggerEvent(x, y);
  		       return true;
         }
    	 else return false;
     }
     
     public void refreshAnimation(){
    	 if(moving){
    		 offset += speed;
    		 if(-2.5 < offset && offset < speed -2.5) ++animState;
    		 if(-0.5 < offset && offset < speed -0.5) ++animState;
    		 animState = animState % numFrames;
  
    		 if(offset >=0){
    			 offset = 0;
    			 moving = false;
    		 }
    	 }
     }
}
