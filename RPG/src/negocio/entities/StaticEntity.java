 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.entities;

import negocio.GameMenus.DialogMenu;
import negocio.action.Action;
import negocio.factories.EventFactory;
import negocio.logic.Dialog;
import negocio.logic.Facing;
import negocio.logic.Game;
import negocio.transfer.EntityUpdateTransfer;


public class StaticEntity extends Entity{
	public StaticEntity(String id, int x, int y, Game game) {
		super(id, x, y, game);
	}

	public void interact() {
		if (dialog != null) {
			game.setMenu(new DialogMenu(this, dialog, game));
			talking = true;
			game.onDialogCreated();
		}
	}


	public void refresh() {
		update();
	}

	@Override
	protected void update() {
		if(logicState != game.getEntityState(id)) {
			logicState = game.getEntityState(id);
			EntityUpdateTransfer t = game.getEntityUpdate(id, "static", logicState); 
		    dialog = new Dialog(t.getDialog());
		    event = EventFactory.createEntityEvent(t.getEvent(), this, game);
		    facing = Facing.values()[t.getFacing()];
		}
	}
	
	public void addOrder(Action order) {
	}

}
