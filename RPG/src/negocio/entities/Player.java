 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.entities;

import negocio.logic.Facing;
import negocio.logic.Game;
import negocio.logic.LogicInput;

public final class Player extends DynamicEntity {
	private Facing ignoredFacing;

	public static final String PLAYER_NAME= "Player";
	
	public Player(Game game) {
		super(PLAYER_NAME ,0, 0, game);
	}
    

	public void takeOrders() {
		if(orders.isEmpty()) {
		if(moving && LogicInput.RUN.isDown())refreshAnimation();
		else if(!moving){
			if (facing.getKey().isDown()) {
				boolean found = false;
	 			for(int i = 0; !found && i < Facing.values().length; ++i) {
					if(Facing.values()[i] != ignoredFacing && Facing.values()[i] != facing
							&& Facing.values()[i].getKey().isDown()) { 
						found = true;
						ignoredFacing = facing;
						facing = Facing.values()[i];
						LogicInput.lock();
					}
				}
				
				advance();
			}
			
			else if (LogicInput.ACTION.isDown()) {
				act = true;
				LogicInput.Menulock();
			} 
			
			else {
				ignoredFacing = null;
				boolean found = false;
				for (int i = 0; !found && i < Facing.values().length; ++i) {
					if (Facing.values()[i].getKey().isDown()) {
						found = true;
						facing = Facing.values()[i];
						LogicInput.lock();
					}
				}
			}
		}
		}
		
	else{
			orders.poll().execute();
	}
	}



	public void refresh() {
		if (act) {
			game.PlayerAction(x + facing.getShiftx(), y + facing.getShifty());
			act = false;
		}
		refreshAnimation();
	}

	public void loadDialog() {}
	public void interact() {}
	protected void update() {
	}
}
