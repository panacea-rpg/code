 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.entities;

import negocio.GameMenus.DialogMenu;
import negocio.factories.EventFactory;
import negocio.logic.Dialog;
import negocio.logic.Facing;
import negocio.logic.Game;
import negocio.transfer.EntityUpdateTransfer;

public class NPC extends DynamicEntity {
	protected int minX;
	protected int maxX;
	protected int minY;
	protected int maxY;

	public NPC(String id, int x, int y, Game game) {
		super(id, x, y, game);
	}

	public void interact() {
		if (!moving) {
			
			prevFacing = facing;
			facing = game.getPlayer().facing.not();
			if (dialog != null) {
				game.setMenu(new DialogMenu(this, dialog, game));
				talking = true;
				game.onDialogCreated();
			}
		}
	}

	public void refresh() {
		if (orders.isEmpty()) {
			update();

			if (!stop  && !talking && game.getRand().nextDouble() < 0.005 && !moving) {
				Facing aux = Facing.values()[game.getRand().nextInt(Facing.values().length)];
				if (minX <= x + aux.getShiftx() && x + aux.getShiftx() <= maxX && minY <= y + aux.getShifty()
						&& y + aux.getShifty() <= maxX && game.isPassable(x + aux.getShiftx(), y + aux.getShifty())
						&& !game.isOccupied(x + facing.getShiftx(), y + facing.getShifty())) {
					facing = aux;
					advance();
				}
			}
		}

		else {
			orders.poll().execute();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		refreshAnimation();

	}

	public void update() {
		if (logicState != game.getEntityState(id)) {
			logicState = game.getEntityState(id);
			EntityUpdateTransfer t = game.getEntityUpdate(id, "dynamic", logicState);
			dialog = new Dialog(t.getDialog());
			event = EventFactory.createEntityEvent(t.getEvent(), this, game);
			facing = Facing.values()[t.getFacing()];
			stop = t.getStop();
			maxX = t.getMaxX();
			maxY = t.getMaxY();
			minX = t.getMinX();
			minY = t.getMinY();
		}
	}

	public int getLogic() {
		return this.logicState;
	}

}
