 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.menu;

import negocio.logic.BusinessLayer;
import negocio.logic.LogicInput;

public abstract class InitialMenu{
	private int numOptions;
	protected int selectedOption;
	protected int selectedConfig;
	private String ID;
	protected BusinessLayer bl;
	public InitialMenu(int numOptions, String ID,BusinessLayer bl) {
		this.numOptions = numOptions;
		this.selectedOption = 0;
		this.selectedConfig = 0;
		this.ID = ID;
		this.bl = bl;
	}
	
	public void takeOrders(){ 

		if(LogicInput.ACTION.isDown()){
			this.action();
    		LogicInput.Menulock();
    	}
    	
    	else if (LogicInput.EXIT.isDown()) {
    		this.exit();
    		LogicInput.Menulock();
    	}
    	
    	else if(LogicInput.UP.isDown()) {
    		if(selectedOption > 0)--selectedOption;
    		LogicInput.Menulock();
    	}
    	           
    	else if(LogicInput.DOWN.isDown()) {
    		if(selectedOption < numOptions - 1)++selectedOption;
    		LogicInput.Menulock();
    	}
    	else if(LogicInput.RIGHT.isDown()) {
    		if(selectedConfig < numOptions - 1)++selectedConfig;
    		LogicInput.Menulock();
    	}
    	           
    	else if(LogicInput.LEFT.isDown()) {
    		if(selectedConfig > 0)--selectedConfig;
    		LogicInput.Menulock();
    	}	
    }
	public abstract void action();
	public abstract void exit();
	public String getID() {
		return ID;
	}
	public int getSelected() {
		return this.selectedOption;
	}


}
