 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.menu;

import negocio.logic.BusinessLayer;
import negocio.menuCommand.ChangeMenuCommand;
import negocio.menuCommand.StartGameCommand;

public class PlayMenu extends InitialMenu {

	public PlayMenu(BusinessLayer bl) {
		super(2, "play", bl);
	}

	@Override
	public void action() {
		new StartGameCommand(bl, this.selectedOption != 0).execute();
	}

	@Override
	public void exit() {
		new ChangeMenuCommand(bl, "MainInitialMenu").execute();
	}

}
