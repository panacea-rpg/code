 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.menu;

import negocio.logic.BusinessLayer;
import negocio.menuCommand.ChangeMenuCommand;
public class MainInitialMenu extends InitialMenu {

	public MainInitialMenu(BusinessLayer bl) {
		super(4, "main", bl);
	}

	@Override
	public void action() {
		switch(this.selectedOption) {
		case 0:
			new ChangeMenuCommand(bl, "play").execute();
			break;
		case 1:
			new ChangeMenuCommand(bl, "config").execute();
			break;
		case 2:
			new ChangeMenuCommand(bl, "ayuda").execute();
			break;
		case 3:
			new ChangeMenuCommand(bl, "creditos").execute();
			break;
		case 4:
			break;
		}
		
	}

	@Override
	public void exit() {
		bl.onExit();
	}

}
