 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.logic;

public enum Facing {
	FRONT(0, LogicInput.DOWN, 0, 1),
	BACK(1,LogicInput.UP, 0, -1),
	RIGHT(2,LogicInput.RIGHT, 1, 0),
	LEFT(3,LogicInput.LEFT, -1, 0);
	
	private final int index;
	private final LogicInput key;
	private final int shiftx;
	private final int shifty;
	
	Facing(int index, LogicInput key, int shiftx, int shifty) {
		this.index = index;
		this.key = key;
		this.shiftx = shiftx;
		this.shifty = shifty;
	}
	public int getInd() {
		return this.index;
	}
	
	public LogicInput getKey() {
		return key;
	}
	
	public int getShiftx() {
		return shiftx;
	}
	
	public int getShifty() {
		return shifty;
	}
	
	public Facing not() {
		if(index % 2 == 0) return Facing.values()[index+1];
		else return Facing.values()[index-1];
	}
	
}
