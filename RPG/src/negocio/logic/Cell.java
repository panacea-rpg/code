 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.logic;

import negocio.entities.Entity;
import negocio.event.Event;

public class Cell {
	private Entity entity;
	private Item item;
	private boolean passable;
	private Event event;
	
	public Cell() {
		this.entity = null;
		this.item = null;
		this.passable = true;
		this.event = null;
	}
	public void setPassable(boolean passable) {
		this.passable = passable;
	}
	
	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}


	public Item getItem() {
		return item;
	}


	public void setItem(Item item) {
		this.item = item;
	}

    public void setEvent(Event event) {
    	this.event = event;
    }
    
    public Event getEvent() {
    	return event;
    }
	
	public boolean isPassable() {
		return passable;
	}
	
	public boolean isOccupied(){
		return (entity != null);
	}

	public void refresh(){
		if(entity != null) entity.refresh();
	}
	
	public void PlayerAction(){
		if(entity != null){
			entity.interact();
		}
	}
}
