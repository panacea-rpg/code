 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.logic;

import java.util.HashMap;

import org.lwjgl.input.Keyboard;

//NOTA: el InputHandler trata a la entrada como si la unica informacion util fuera presionar una tecla
public enum LogicInput {
	UP(Keyboard.KEY_W,Keyboard.KEY_UP), 
	DOWN(Keyboard.KEY_S,Keyboard.KEY_DOWN), 
	RIGHT(Keyboard.KEY_D,Keyboard.KEY_RIGHT), 
	LEFT(Keyboard.KEY_A,Keyboard.KEY_LEFT), 
	ACTION(Keyboard.KEY_SPACE),
	BACK(Keyboard.KEY_BACK), 
	START(Keyboard.KEY_RETURN), 
	INVENTORY(Keyboard.KEY_E), 
	RUN(Keyboard.KEY_RSHIFT,Keyboard.KEY_LSHIFT), 
	EXIT(Keyboard.KEY_ESCAPE), 
	ERASE(Keyboard.KEY_BACK);

	private int key;
	private int altKey;
	private int counter;
	private boolean isDown;

	LogicInput(int key) {
		this.counter = 0;
		this.key = key;
		this.altKey = -1;
		this.isDown = false;
	}
	LogicInput(int key,int altKey) {
		this.counter = 0;
		this.key = key;
		this.altKey = altKey;
		this.isDown = false;
	}

	/**
	 * Each cycle checks for new input
	 */
	public static void updateInput(HashMap<Integer, Boolean> inputMap) {
		for (LogicInput ih: LogicInput.values()) {
			if(inputMap.containsKey(ih.key))
				ih.isDown = inputMap.get(ih.key);

			if(inputMap.containsKey(ih.altKey))
				ih.isDown =inputMap.get(ih.altKey);			
		}
		tick();
	}

	private static void tick() {
		for (LogicInput ih : LogicInput.values()) {
			if (ih.counter > 0)
				--ih.counter;
		}
	}

	public boolean isDown() {
		return isDown && this.counter == 0;
	}

	public static void lock() {
		for (LogicInput ih : LogicInput.values())
			ih.counter = Game.FPS / 10;
	}

	public static void Menulock() {
		for (LogicInput ih : LogicInput.values())
			ih.counter = Game.FPS / 7;
	}

}
