 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.transfer;

import java.util.List;

public class DialogTransfer {
	private String id;
	private int logicState;
	private List<String> conversation;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getLogicState() {
		return logicState;
	}
	public void setLogicState(int logicState) {
		this.logicState = logicState;
	}
	public List<String> getConversation() {
		return conversation;
	}
	public void setConversation(List<String> conversation) {
		this.conversation = conversation;
	}
	
}
