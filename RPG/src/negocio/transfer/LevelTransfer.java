 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.transfer;

import java.util.List;

import negocio.event.TransferCellEvent;
import presentacion.render.AnimationTransfer;

public class LevelTransfer {
	private boolean[][] frontiers;
	private String levelName;
	private int levelWidth;
	private int levelHeight;
	private AnimationTransfer mapTextures[];
	private int ciclos;
	private List<EntityLevelTransfer> entitiesData;
	private List<TransferCellEvent> events;
	private String sound;


	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public int getLevelWidth() {
		return levelWidth;
	}

	public void setLevelWidth(int levelWidth) {
		this.levelWidth = levelWidth;
	}

	public int getLevelHeight() {
		return levelHeight;
	}

	public void setLevelHeight(int levelHeight) {
		this.levelHeight = levelHeight;
	}

	public AnimationTransfer[] getMapTextures() {
		return mapTextures;
	}

	public void setMapTextures(AnimationTransfer[] mapTextures) {
		this.mapTextures = mapTextures;
	}

	public int getCiclos() {
		return ciclos;
	}

	public void setCiclos(int ciclos) {
		this.ciclos = ciclos;
	}

	public boolean[][] getFrontiers() {
		return frontiers;
	}

	public void setFrontiers(boolean[][] frontiers) {
		this.frontiers = frontiers;
	}

	public List<EntityLevelTransfer> getEntitiesData() {
		return entitiesData;
	}

	public void setEntitiesData(List<EntityLevelTransfer> entitiesData) {
		this.entitiesData = entitiesData;
	}

	public List<TransferCellEvent> getEvents() {
		return events;
	}

	public void setEvents(List<TransferCellEvent> events) {
		this.events = events;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}

}
