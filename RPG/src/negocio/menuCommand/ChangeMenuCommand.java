 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.menuCommand;

import negocio.logic.BusinessLayer;
import negocio.menu.ConfigMenu;
import negocio.menu.HelpInitialMenu;
import negocio.menu.MainInitialMenu;
import negocio.menu.PlayMenu;

public class ChangeMenuCommand extends Command {
	private String destMenu;

	public ChangeMenuCommand(BusinessLayer bl,String destMenu) {
		super(bl);
		this.destMenu = destMenu;
	}
	
	@Override
	public void execute() {
		if(destMenu.equals("MainInitialMenu"))
			this.bl.setMenu(new MainInitialMenu(bl));
		else  if(destMenu.contentEquals("config"))
			this.bl.setMenu(new ConfigMenu(bl));
		else if(destMenu.contentEquals("play"))
			this.bl.setMenu(new PlayMenu(bl));
		else this.bl.setMenu(new HelpInitialMenu(bl,destMenu));
		
	}

}
