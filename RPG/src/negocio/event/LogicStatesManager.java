 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.event;

import java.util.Map;

public class LogicStatesManager {
	private Map<String, Integer> entitiesStates;

	public LogicStatesManager(Map<String, Integer> entitiesStates) {
		this.entitiesStates = entitiesStates;
	}

	public Map<String, Integer> getEntitiesStates() {
		return entitiesStates;
	}

	public int getState(String key) {
		if (!entitiesStates.containsKey(key))
			entitiesStates.put(key, 0);
		return entitiesStates.get(key);
	}

	public void setState(String key, int state) {
		entitiesStates.put(key, state);
	}
	public boolean isNewGame() {
		for(Map.Entry<String, Integer> m:entitiesStates.entrySet()) {
			if(m.getValue() != 0) return false;
		}
		return true;
	}

}
