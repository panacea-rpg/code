 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.event;

import negocio.entities.Entity;
import negocio.entities.Player;
import negocio.logic.Facing;
import negocio.logic.Game;

public class WarpPort extends EntityEvent {
	public WarpPort(Entity entity, Game game) {
		super("WarpPort" , entity, game);
		
	}

	public void execute() {
		game.setCrazyMode(true);
		Player p = game.getPlayer();
		game.changeLevel("Level0", p.getX(), p.getY());
		p.setFacing(Facing.RIGHT);
		game.changeLevel("port", 52, 52);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		game.getPlayer().setAct(true);
	}
    
}
