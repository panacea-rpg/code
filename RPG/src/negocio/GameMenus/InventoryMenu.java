 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.GameMenus;

import negocio.logic.Game;
import negocio.logic.Inventory;
import negocio.logic.Item;
import negocio.logic.LogicInput;
import negocio.transfer.ItemTransfer;

public class InventoryMenu extends Menu {
	private Item[] matrix;
	private int x;
	private DialogMenu menu;

	public InventoryMenu(Game game, Inventory inventory) {
		super(game);
		this.x = 0;
		this.matrix = inventory.getItems();
	}

	public void takeOrders() {
		if (menu != null) {
			boolean cerrado = menu.takeInventoryOrders(matrix[x].getEvent());
			if (cerrado) {
				menu = null;
				game.onDialogDestroyed();
			}
		} else {
			if (LogicInput.EXIT.isDown() || LogicInput.BACK.isDown() || LogicInput.INVENTORY.isDown() ){
				game.setMenu(null);
				game.onInventoryDestroyed();
				LogicInput.Menulock();
			} else if (LogicInput.UP.isDown()) {
				--x;
				LogicInput.Menulock();
			} else if (LogicInput.DOWN.isDown()) {
				++x;
				LogicInput.Menulock();
			} else if (LogicInput.ACTION.isDown() && matrix.length != 0 && matrix[x].getDialog() != null ) {
				menu = new DialogMenu(null, matrix[x].getDialog(), game);
				game.onDialogCreated();
				LogicInput.Menulock();
			}
			if (matrix.length != 0)
				x = x % matrix.length;
			else x = 0;
		}
	}

	public ItemTransfer getInventoryItems() {
		String l[] = new String[matrix.length];
		for (int i = 0; i < matrix.length; ++i) {
			if (matrix[i] == null)
				l[i] = null;
			else
				l[i] = matrix[i].getId();
		}
		ItemTransfer t = new ItemTransfer();
		t.setItems(l);
		t.setSelectedOption(x);
		return t;
	}

	public String getDialogMenuTransfer() {
		if (menu != null)
			return menu.getDialogMenuTransfer();
		else
			return null;
	}

	public void refresh() {
	}
}
