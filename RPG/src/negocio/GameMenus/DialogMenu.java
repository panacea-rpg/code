 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.GameMenus;

import negocio.entities.Entity;
import negocio.event.Event;
import negocio.logic.Dialog;
import negocio.logic.Game;
import negocio.logic.LogicInput;

public class DialogMenu extends Menu {
	private Entity interloc;
	private Dialog dialog;
	private int selectedOption;
	
    public DialogMenu(Entity interloc, Dialog dialog, Game game){
    	super(game);
    	this.interloc = interloc;
    	this.dialog = dialog;
    	this.selectedOption = 0;
    }

    
    public void takeOrders(){ 
    	if(LogicInput.ACTION.isDown()) {
    		if(selectedOption == dialog.getConversation().size() - 1) {
    			game.setMenu(null);
    			if(interloc != null)interloc.setTalking(false);
    			if(interloc != null)if(interloc.getPrevFacing()!= null)interloc.setFacing(interloc.getPrevFacing());
    			game.onDialogDestroyed();
    			if(interloc != null)if(interloc.getEvent() != null) interloc.getEvent().execute();
    		}else {
    			selectedOption++;
    			try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		LogicInput.Menulock();
    	}
    	else if (LogicInput.BACK.isDown() || LogicInput.EXIT.isDown()) {
    		game.setMenu(null);
    		if(interloc != null)interloc.setTalking(false);
    		game.onDialogDestroyed();
    		LogicInput.Menulock();
    	}
    	
    }
    public boolean takeInventoryOrders(Event event) {
    	if(LogicInput.ACTION.isDown()) {
    		if(selectedOption == dialog.getConversation().size() - 1) {
    			LogicInput.Menulock();
    			if(event!=null)event.execute();
    			return true;
    		}else {
    			selectedOption++;
    			try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		LogicInput.Menulock();
    	}
    	else if (LogicInput.BACK.isDown() || LogicInput.EXIT.isDown()||LogicInput.INVENTORY.isDown()) {
    		LogicInput.Menulock();
    		return true;
    	}
    	return false;
    }
    public String getDialogMenuTransfer() {
    	return dialog.getConversation().get(this.selectedOption);
    }
    public void refresh() {
    	
    }
}
