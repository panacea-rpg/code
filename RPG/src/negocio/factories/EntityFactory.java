 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.factories;

import negocio.entities.Entity;
import negocio.entities.NPC;
import negocio.entities.StaticEntity;
import negocio.logic.Game;

public class EntityFactory {
    public static Entity createEntity(String id, String type, int x, int y, Game game) {
    	switch(type) {
    	case "static": return new StaticEntity(id, x, y, game);
    	case "dynamic": return new NPC(id,x,y, game);
    	default: return null;
    	}
    }
}
