 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package negocio.factories;

import negocio.entities.Entity;
import negocio.event.AbuTransition;
import negocio.event.AkillQuest;
import negocio.event.BasinObtained;
import negocio.event.BathTime;
import negocio.event.BlockFaro;
import negocio.event.CellEvent;
import negocio.event.CrazyModeOn;
import negocio.event.EndGame;
import negocio.event.EntityEvent;
import negocio.event.GaiaAwakening;
import negocio.event.GiveKey;
import negocio.event.GiveLetter;
import negocio.event.GiveMirror;
import negocio.event.IncreaseAkill;
import negocio.event.IncreaseCaptain;
import negocio.event.IncreaseState;
import negocio.event.ItemEvent;
import negocio.event.QuestForABasin;
import negocio.event.ReadLetter;
import negocio.event.SerapeumLastEvent;
import negocio.event.SerapeumTransition;
import negocio.event.ToFaro;
import negocio.event.TransferCellEvent;
import negocio.event.Transition;
import negocio.event.WarpPort;
import negocio.logic.Game;
import negocio.logic.Item;

public class EventFactory {
	public static CellEvent createCellEvent(TransferCellEvent t, Game game) {
		switch (t.getName()) {
		case "Transition":
			if (t.getDestLevel().contentEquals("Serapeum"))
				return new SerapeumTransition(t.getX(), t.getY(), game, t.getDestLevel(), t.getDestX(), t.getDestY());
			else if(t.getDestLevel().contentEquals("AbuDownstairs")) {
				return new AbuTransition(t.getX(), t.getY(), game, t.getDestX(), t.getDestY());
			}
			else
				return new Transition(t.getX(), t.getY(), game, t.getDestLevel(), t.getDestX(), t.getDestY());
		case "GaiaAwakening":
			return new GaiaAwakening(t.getX(), t.getY(), game);
		case "BlockFaro":
			return new BlockFaro(t.getX(), t.getY(), game);
		case "":
			return null;
		default:
			throw new IllegalArgumentException();
		}
	}

	public static EntityEvent createEntityEvent(String id, Entity entity, Game game) {
		switch (id) {
		case "IncreaseState":
			return new IncreaseState(entity, game);
		case "CrazyModeOn":
			return new CrazyModeOn(entity, game);
		case "WarpPort":
			return new WarpPort(entity, game);
		case "QuestForABasin":
			return new QuestForABasin(entity, game);
		case "BasinObtained":
			return new BasinObtained(entity, game);
		case "BathTime":
			return new BathTime(entity, game);
		case "ToFaro":
			return new ToFaro(entity, game);
		case "SerapeumLastEvent":
			return new SerapeumLastEvent(entity,game);
		case "EndGame":
			return new EndGame(entity, game);
		case "GiveLetter":
			return new GiveLetter(entity, game);
		case "AkillQuest":
			return new AkillQuest(entity,game);
		case "GiveKey":
			return new GiveKey(entity,game);
		case "GiveMirror":
			return new GiveMirror(entity,game);
		case "":
			return null;
		default:
			throw new IllegalArgumentException();
		}
	}

	public static ItemEvent createItemEvent(String id, Item item, Game game) {
		switch (id) {
		case "ReadLetter":
			return new ReadLetter(item, game);
		case "IncreaseCaptain":
			return new IncreaseCaptain(item, game);
		case "IncreaseAkill":
			return new IncreaseAkill(item, game);
		case "":
			return null;
		default:
			throw new IllegalArgumentException();
		}
	}
}
