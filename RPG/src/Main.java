 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import java.io.File;

import presentacion.render.View;
public class Main {

	public static void main(String[] args) {
		//Para la retina? Yes
		//-Dorg.lwjgl.opengl.Display.enableHighDPI=false
		System.setProperty("org.lwjgl.opengl.Display.enableHighDPI", "false");
		//Para los jar
		System.setProperty("org.lwjgl.librarypath", new File("lib/natives").getAbsolutePath());
		View view = new View();
		view.init();
	}
}
