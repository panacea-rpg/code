 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.sound;

import java.io.IOException;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Sound {
	private Audio audio;
	private String name;
	public Sound(String name) {
		this.name = name;
		try {
			audio	= AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("res/audio/" + name + ".wav"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * The audio is played in independent thread
	 */
	public void play() {
		try {
			new Thread() {
				public void run() {
					//tambien existe playAsMusic
					audio.playAsSoundEffect(1.0f, 1.0f, false);
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		//SoundStore.get().poll(0);
	}
	public void loop() {
		try {
			new Thread() {
				public void run() {
					//tambien existe playAsMusic
					audio.playAsSoundEffect(1.0f, 1.0f, true);
				}
			}.start();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		//SoundStore.get().poll(0);
	}
	public boolean isPlaying() {
		return audio.isPlaying();
	}
	public void stop() {
		audio.stop();
	}

	public String getName() {
		return name;
	}

}
