 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion;

import java.util.HashMap;

import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.Texture;

import negocio.logic.BusinessLayer;
import negocio.logic.Game;
import negocio.transfer.EntityRenderTransfer;
import negocio.transfer.ItemTransfer;
import presentacion.input.InputHandler;
import presentacion.input.MouseHandler;
import presentacion.render.AnimationTransfer;
import presentacion.render.RenderObserver;
import presentacion.render.View;

public class Controller {
	private View render;
	private BusinessLayer bl;
	boolean running;

	public Controller(View render) {
		this.render = render;
		bl = new BusinessLayer(render);
	}

	public void run() {

		MouseHandler.init();
		running = true;
		while (running) {
			HashMap<Integer, Boolean> inputMap = InputHandler.getInput();
			bl.updateInput(inputMap);

			if (Display.isCloseRequested())
				running = false;
			if (!Display.isActive())
				inputMap = new HashMap<Integer, Boolean>();
			// Mouse
			MouseHandler.processEvents();

			// Logic
			bl.refresh();

			// Render
			render.refresh();

			// window updates
			Display.update();
			Display.sync(Game.FPS);
		}
		MouseHandler.destroy();
		render.destroy();
		AL.destroy();

	}
	
	public void removeObserverMenu() {
		bl.setMenuObserver(null);
	}

	public void registerObserverMenu(RenderObserver rs) {
		bl.setMenuObserver(rs);
	}

	public EntityRenderTransfer[][] getEntitiesRenderings() {
		return bl.getEntitiesRenderings();
	}

	public EntityRenderTransfer getPlayerRenderin() {
		return bl.getPlayerRendering();
	}

	public void updateLevelFrontiers(boolean[][] map, String levelName) {
		bl.updateLevelFrontiers(map, levelName);

	}

	public void loadAnimations(AnimationTransfer[] anims) {
		bl.loadAnimations(anims);
	}

	public String getDialogMenuTransfer() {
		return bl.getDialogMenuTransfer();
	}

	public void onExit() {
		running = false;
	}

	public void getEncapuchado(HashMap<String, Texture[][]> npcTextures) {
		bl.getEncapuchado(npcTextures);
	}

	public ItemTransfer getInventoryTransfer() {
		return bl.getInventoryTransfer();
	}
	
}
