 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import org.newdawn.slick.opengl.Texture;
//https://www.oracle.com/technetwork/java/transferobject-139757.html
public class Animation extends AnimationTransfer {
    private int actualFrame;

	public Animation(AnimationTransfer transfer) {
		super(transfer.getPath(),transfer.getNumFrames());
		this.frames = transfer.getFrames();
    	this.actualFrame = 0;
	}
    public Animation(String path, int numFrames){
    	super(path,numFrames);
    }
    public Animation(String path) {
		super(path);
	}
    public Animation(String p1,String p2,String p3) {
		super(p1 + "/" + p2 + "/"  + p3);
	}
    
	public Texture getFrame() {
    	return frames[actualFrame];
    }
    public Texture getFrame(int ind) {
    	return frames[ind];
    }
    public void next() {
    	actualFrame = (actualFrame + 1) % this.numFrames;
    }
    public int getImageWidth() {
    	 return frames[0].getImageWidth();
    }
    public int getImageHeight() {
    	return frames[0].getImageHeight();
    }
    
	public void play() throws InterruptedException{};
}
