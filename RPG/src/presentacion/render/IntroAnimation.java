 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import org.lwjgl.opengl.Display;


public class IntroAnimation extends Animation {
	private View render;

	public IntroAnimation(View render) {
		super("animations","intro","keyboard");
		this.numFrames = 1;
		this.render = render;
		AnimationTransfer[] anims = { this};
		render.loadAnimations(anims);
	}

	public void play(){
		render.setScalingToFillScreen(frames[0].getImageHeight(), frames[0].getImageWidth());
		render.renderMap(frames[0]);
		Display.update();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
