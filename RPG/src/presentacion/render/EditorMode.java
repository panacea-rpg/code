 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import negocio.logic.LogicInput;
import presentacion.input.MouseHandler;

public class EditorMode {
	private View render;
	private boolean map[][];
	private Animation pincel = new Animation("editorRes/pincel");
	private Animation frontier = new Animation("editorRes/frontier");
	private Animation cell = new Animation("editorRes/cell");
	private LevelRender renderLevel;

	private String actualLevel;
	private TextRender textRender;
	private int charPerLine;

	public EditorMode(View render, LevelRender renderLevel, boolean[][] map) {
		this.render = render;
		this.renderLevel = renderLevel;
		this.map = map;
		AnimationTransfer[] anims = { pincel, frontier, cell };
		render.loadAnimations(anims);
		
		this.textRender = new TextRender("arbutus",24f);
		this.charPerLine = (int) render.getWIDTH()/20;
	}

	private void renderDialog(String text) {
		for(int i = 0; i<text.length() ; i+= charPerLine) {
			this.textRender.drawText(260,950 + i/charPerLine * 30,text.substring(i, Math.min(i+charPerLine,text.length())));
		}	
	}

	public void renderEditor() {
		divisionCasillar();
		drawCell(pincel, getMousexPos(), getMouseyPos());
		drawMapCells();
		processInput();
		render.originCamera();
		this.renderDialog("Awesome FrontierEditor Vol. 1                         -Press ENTER to save-");
	}

	private void divisionCasillar() {
		for (int i = 0; i < map.length; ++i) {
			for (int j = 0; j < map[0].length; ++j) {
				drawCell(cell, i, j);
			}
		}
	}

	private void processInput() {
		if (MouseHandler.isLeftDown()) {
			try {
				map[getMousexPos()][getMouseyPos()] = !map[getMousexPos()][getMouseyPos()];
				System.out.println("No salido de rango: " + getMousexPos() + " " + getMouseyPos());
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("Salido de rango: " + getMousexPos() + " " + getMouseyPos());
			}
		}
		if (LogicInput.START.isDown()) {
			guardarEstado();
		}

	}

	private void guardarEstado() {
		render.updateLevelFrontiers(map, this.actualLevel);
	}

	private int getMousexPos() {
		float screenX = MouseHandler.getX()
				+ renderLevel.fromRenderingToScreenCoordX(render.getPlayerRendering()) * render.getScaleWidth()
				- render.getWIDTH() / 2.0f;
		return (int) (screenX / (float) (cell.getImageHeight() * render.getScaleWidth()));
	}

	private int getMouseyPos() {
		float screenY = +render.getHEIGHT() / 2.0f - MouseHandler.getY()
				+ renderLevel.fromRenderingToScreenCoordY(render.getPlayerRendering()) * render.getScaleHeight();
		return (int) (screenY / (float) (render.getScaleHeight() * cell.getImageHeight()));
	}

	private void drawMapCells() {
		for (int i = 0; i < map.length; ++i) {
			for (int j = 0; j < map[0].length; ++j) {
				if (!map[i][j])
					drawCell(frontier, i, j);
			}
		}
	}

	private void drawCell(Animation anim, int x, int y) {
		float i = x * anim.getImageWidth();
		float j = y * anim.getImageHeight();
		render.renderTexture(anim, i, j);
	}

}
