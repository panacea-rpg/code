 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import org.newdawn.slick.Color;

public class DialogRender {
	private Animation pergamino = new Animation("menus","dialog","pergamin");
	private View render;
	private TextRender textRender;
	private int charPerLine;
	private static final Color c = Color.black;
	private final int initX;
	private final int initY;
	private final int spacing;

	public DialogRender(View render) {
		this.render = render;
		AnimationTransfer[] anims = { pergamino};
		render.loadAnimations(anims);
		this.textRender = render.getTextRender();
		this.charPerLine = (int) render.getWIDTH()*2/(14*3);
		this.initX = (int) (render.getWIDTH())*2/13;
		this.initY = (int) (render.getHEIGHT())*7/8;
		this.spacing = 30;
	}
	
	public void render(String s) {
		render.originCamera();
		float xPos = 0;
		float yPos = render.getHEIGHT()*2/(3*render.getScaleHeight());
		float scaleWidth = render.getWIDTH()/(pergamino.getImageWidth() );
		float scaleHeight = render.getHEIGHT()/(pergamino.getImageHeight()*3);
		render.renderTextureWithScaling(pergamino.getFrame(), xPos, yPos, scaleWidth, scaleHeight);
		renderDialog(s);
		//Para solucionar un pequeño bug
		this.textRender.drawText(0, 0, "", Color.white);
		if(render.isCrazyOn())this.textRender.drawText(0, 0, "", render.getCrazyColor());
	}
	
	public void renderDialog(String text) {
		int n = render.isCrazyOn() ? (int) render.getWIDTH()/(14*3) : charPerLine;
		if(text.length()/n == 0) {
			this.textRender.drawText(this.initX,initY,text,c);
			return;
		}
		int[] array = new int[(text.length()+n-1)/n];
		int j = 0;
		for(int i = n; i < text.length(); ) {
			int index = i;
			while(index >= 0 && text.charAt(index) != ' ') index--;
			array[j] = index;
			i = index + n;
			j++;
		}
		array[array.length-1] = text.length();
		if(array.length > 0)
			this.textRender.drawText(this.initX,initY,text.substring(0, array[0]),c);
		for(int i = 1; i < array.length ; i++) {
			this.textRender.drawText(this.initX,initY +i*spacing,text.substring(array[i-1]+1, array[i]),c);
		}
	}
	
	public void renderAnswers(String text, int index) {
		int n = render.isCrazyOn() ? (int) render.getWIDTH()/(14*3) : charPerLine;
		for(int i = 0; i<text.length() ; i+= n) {
			this.textRender.drawText(this.initX,initY + +index*spacing,text.substring(i, Math.min(i+n,text.length())),c);
		}	
	}
	
	public void renderSelectedAnswer(String text, int index) {
		int n = render.isCrazyOn() ? (int) render.getWIDTH()/(14*3) : charPerLine;
		for(int i = 0; i<text.length() ; i+= n) {
			this.textRender.drawText(initX,initY + i/n * spacing +index*spacing,("-" + text).substring(i, Math.min(i+n,text.length())),c);
		}	
	}

}
