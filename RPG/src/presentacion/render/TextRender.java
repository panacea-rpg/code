 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

public class TextRender {
	private TrueTypeFont font2;
	public TextRender(String font, float fontsize) {
		this.initFonts(font, fontsize);
	}

	private void initFonts(String font, float fontsize) {
		
        InputStream is = null;
        /* TODO */
       // load font from file
       try {
         is = org.newdawn.slick.util.ResourceLoader.getResourceAsStream("res/fonts/"+font+".ttf");
            
       } catch (Exception e) {
           e.printStackTrace();
       }

       Font awtFont2 = null;
		try {
			awtFont2 = Font.createFont(Font.TRUETYPE_FONT, is);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       awtFont2 = awtFont2.deriveFont(fontsize); // set font size
        font2 = new TrueTypeFont(awtFont2, true);
	}

	/**
	 * Font drawer. USING DEPRECATED METHODS
	 */
	@SuppressWarnings("deprecation")
	public void drawText(int i,int j,String s) {
		
         font2.drawString(i, j, s);
        
	}
	public void drawText(int i,int j,String s,Color c) {
		
        font2.drawString(i, j, s,c);
       
	}
}
