 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import org.newdawn.slick.opengl.Texture;

public class AnimationTransfer {
	protected Texture[] frames;
    protected int numFrames;
    private String path;
    
    public AnimationTransfer(String path,int numFrames){
    	frames = new Texture[numFrames];
    	this.numFrames = numFrames;
    	this.path = path;
    }
   /* public AnimationTransfer(String name, int numFrames) {
    	this(name,numFrames,"animations");
    }*/

    public AnimationTransfer(String path) {
    	frames  = new Texture[1];
    	this.numFrames = 1;
    	this.path = path;
    }
    //constructora sin parametros
	public AnimationTransfer() {
	}

	public Texture[] getFrames() {
		return frames;
	}
	public void setFrames(Texture[] frames) {
		this.frames = frames;
	}
	public int getNumFrames() {
		return numFrames;
	}
	public void setNumFrames(int numFrames) {
		this.numFrames = numFrames;
	}
	/*public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}*/
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
    
}
