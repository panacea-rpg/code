 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import java.util.HashMap;

import org.newdawn.slick.opengl.Texture;

public class InitialMenuRender implements RenderObserver {
	private View render;
	private int selectedOption;
	private String ID;
	private HashMap<String, Animation> map;

	public InitialMenuRender(View render) {
		this.ID = "main";
		this.render = render;
		map = new HashMap<String, Animation>();
		map.put("main", new Animation("menus/main", 4));
		AnimationTransfer[] anims = { map.get("main") };
		render.loadAnimations(anims);
		this.selectedOption = 0;
	}

	public void render() {
		Texture actualFrame = map.get(ID).getFrame(this.selectedOption);
		render.setScalingToFillScreen(actualFrame.getImageHeight(), actualFrame.getImageWidth());
		render.originCamera();
		render.renderMap(actualFrame);
	}

	@Override
	public void onLevelCreated(boolean[][] map, Animation[] mapTextures, HashMap<String, Texture[][]> npcTextures,String levelName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onInitialMenuChanged(String ID, int selectedOption) {
		this.selectedOption = selectedOption;
		if (!this.ID.equals(ID)) {
			if (!map.containsKey(ID)) {
				switch(ID) {
				case "submenu1":map.put("submenu1", new Animation("menus/submenu1", 4));break;
				case "submenu2":map.put("submenu2", new Animation("menus/submenu2", 3));break;
				case "ayuda":map.put("ayuda", new Animation("menus/ayuda", 1));break;
				case "config":map.put("config", new Animation("menus/conf", 4));break;
				case "creditos":map.put("creditos", new Animation("menus/creditos", 1));break;
				case "play":map.put("play", new Animation("menus/play", 2));break;
				
				}
				AnimationTransfer[] anims = { map.get(ID) };
				render.loadAnimations(anims);
			}
			this.ID = ID;
		}
	}

	@Override
	public void executeIntroAnimation() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCrazyActivated() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCrazyDesactivated() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAudioChanged(String audio) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onExit() {
		// TODO Auto-generated method stub
		
	}
}
