 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import java.util.HashMap;

import org.newdawn.slick.opengl.Texture;

public interface RenderObserver {
	//map es la matriz de fronteras.
	//mapTextures la matriz de Animation que el BL pide al DAO que cargue a partir de AnimationTransfer
	//npcTextures es el mapa de texturas de npc y entities.
	public void onLevelCreated(boolean map[][], Animation mapTextures[],HashMap<String,Texture[][]> npcTextures, String levelName);
	public void onInitialMenuChanged(String ID,int selectedOption);
	public void executeIntroAnimation();
	public void onCrazyActivated();
	public void onCrazyDesactivated();
	public void onAudioChanged(String audio);
	public void onExit();
	
}
