 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;


import java.util.HashMap;

import org.newdawn.slick.opengl.Texture;

import negocio.transfer.EntityRenderTransfer;

public class LevelRender {

	private int levelWidth;
	private int levelHeight;
	private Animation mapTextures[];
	private int ciclos;
	private View render;
	private HashMap<String, Texture[][]> npcTextures;
	private EditorMode EM;
	private final boolean isEditorOn = true;
	private float fov;

	public LevelRender(View render, Animation mapTextures[],HashMap<String, Texture[][]> npcTextures,boolean[][] map, String levelName,int fov) {
		if(levelName.contentEquals("DownstairsFaro"))this.fov = 3.5f + fov*3;
		else this.fov = 7 + fov*3;
		this.render = render;
		this.mapTextures = mapTextures;
		this.ciclos = 0;
		this.levelHeight = this.levelWidth = 0;
		for (int i = 0; i < mapTextures.length; ++i) {
			if (mapTextures[i] != null) {
				this.levelHeight = mapTextures[i].getImageHeight() / render.getCellSize();
				this.levelWidth = mapTextures[i].getImageWidth() / render.getCellSize();
				break;
			}
		}
		if(isEditorOn) this.EM = new EditorMode(render, this, map);
		this.npcTextures = npcTextures;
		render.getEncapuchado(npcTextures);
	}

	public void renderLevel() {
		EntityRenderTransfer playerRender = render.getPlayerRendering();
		float x = fromRenderingToScreenCoordX(playerRender);
		float y = fromRenderingToScreenCoordY(playerRender);
		render.centerCameraOn(x, y);
		setScalingToMovingCamera();

		renderBoard();
		if(isEditorOn)EM.renderEditor();
	}

	private void renderBoard() {
		ciclos++;
		EntityRenderTransfer board[][] = render.getEntitiesRenderings();
		for (int j = 0; j < this.levelHeight; ++j) {
			if (mapTextures[j] != null) {
				if ((ciclos) % 30 == 0)
					mapTextures[j].next();
				renderMap(mapTextures[j].getFrame());
			}

			for (int i = 0; i < this.levelWidth; ++i) {
				if (board[i][j] != null && npcTextures.get(board[i][j].getId()) != null) {
					drawEntity(board[i][j]);
				}
			}
		}
	}

	private void setScalingToMovingCamera() {
		render.setScaleHeight(render.getHEIGHT() / (fov * render.getCellSize()));
		render.setScaleWidth(render.getScaleHeight());
	}

	protected float fromRenderingToScreenCoordX(EntityRenderTransfer rend) {
		float i = fromLevelToScreenCoord(rend.getX());
		// offset when entity is moving
		float offset1 = rend.getOffset() * render.getCellSize() / 4.0f;
		// offset to center the entity in the middle of the screen
		float offset2 = render.getCellSize() / 2.0f;
		i += offset1 * rend.getShiftX() + offset2;
		return i;
	}

	protected float fromRenderingToScreenCoordY(EntityRenderTransfer rend) {
		float j = fromLevelToScreenCoord(rend.getY()) - getTextureFromTransfer(rend).getImageHeight() / 2
				+ render.getCellSize();
		float offset = rend.getOffset() * render.getCellSize() / 4.0f;
		j += offset * rend.getShiftY();
		return j;
	}

	private void drawEntity(EntityRenderTransfer rend) {
		float i = rend.getX() * render.getCellSize();
		float offset = rend.getOffset() * render.getCellSize() / 4.0f;
		i += offset * rend.getShiftX();
		float j = rend.getY() * render.getCellSize() - getTextureFromTransfer(rend).getImageHeight()
				+ render.getCellSize();
		j += offset * rend.getShiftY();
		if(render.isCrazyOn() && !rend.getId().contentEquals("Player")) {
			render.renderTexture(npcTextures.get("Sectarian")[rend.getInd()][rend.getAnimState()], i, j);
		}else render.renderTexture(getTextureFromTransfer(rend), i, j);
	}
	
	private void renderMap(Texture texture) {
		render.renderTexture(texture, 0, 0);
	}

	private Texture getTextureFromTransfer(EntityRenderTransfer rend) {
		return npcTextures.get(rend.getId())[rend.getInd()][rend.getAnimState()];
	}

	private float fromLevelToScreenCoord(int coord) {
		return coord * render.getCellSize();
	}
	public float getFov() {
		return fov;
	}

}
