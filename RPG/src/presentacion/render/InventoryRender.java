 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;


import java.util.HashMap;
import java.util.Map;

import negocio.transfer.ItemTransfer;

public class InventoryRender {
	private Animation inventory = new Animation("menus","inventory","inventory");
	private Animation filter = new Animation("menus","inventory","filtro");
	private Animation cell = new Animation("editorRes/cell");
	private View render;
	private static Map<String,Animation> map = new HashMap<String, Animation>();
	private static final float mult = 1.5f;

	public InventoryRender(View render) {
		this.render = render;
		AnimationTransfer[] anims = { inventory,cell,filter};
		render.loadAnimations(anims);
	}
	
	public void render(ItemTransfer t) {
		String[] items = t.getItems();
		render.originCamera();
		float xPos = render.getWIDTH()/ render.getScaleWidth() - inventory.getImageWidth()*mult;
		float yPos = 0;
		float scaleWidth = render.getScaleWidth()*mult;
		float scaleHeight = render.getHEIGHT()/(render.getFov()/15.0f*inventory.getImageHeight())*mult;
		render.renderTextureWithScaling(filter.getFrame(), 0, 0, scaleWidth, scaleHeight);
		render.renderTextureWithScaling(inventory.getFrame(), xPos, yPos, scaleWidth, scaleHeight);
		renderSelectedOption(scaleWidth,scaleHeight,t.getSelectedOption());
		for(int i = 0; i < items.length;++i) {
			renderOption(scaleWidth, scaleHeight, i, items[i]);
		}
	}
	private void renderOption(float scaleWidth,float scaleHeight,int i,String id) {
		float xPos =  (render.getWIDTH()/ render.getScaleWidth() - cell.getImageWidth()*mult);
		float yPos = cell.getImageWidth() * i*mult;
		if(!map.containsKey(id)) load(id);
		render.renderTextureWithScaling(map.get(id).getFrame(),xPos, yPos, scaleWidth, scaleHeight);
	}

	private void renderSelectedOption(float scaleWidth,float scaleHeight,int i) {
		float xPos =  (render.getWIDTH()/ render.getScaleWidth() - cell.getImageWidth()*mult);
		float yPos = cell.getImageWidth() * i*mult;
		render.renderTextureWithScaling(cell.getFrame(),xPos, yPos, scaleWidth, scaleHeight);
	}
	
	private void load(String id) {
		map.put(id, new Animation("menus","inventory",id));
		AnimationTransfer[] anims = {map.get(id)};
		render.loadAnimations(anims);
	}
}
