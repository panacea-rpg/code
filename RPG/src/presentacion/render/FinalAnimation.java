 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.render;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;

public class FinalAnimation extends Animation {
	private View render;
	private TextRender textRender;
	private static final int r = 255;
	private static final int g = 255;
	private static final int b = 255;
	private int a;
	public FinalAnimation(View render) {
		super("menus/end");
		this.a = 0;
		this.numFrames = 1;
		textRender = new TextRender("arbutus",1);
		this.render = render;
		AnimationTransfer[] anims = { this};
		render.loadAnimations(anims);
	}

	public void play(){
		render.setScalingToFillScreen(frames[0].getImageHeight(), frames[0].getImageWidth());
		render.originCamera();
		for(int i = 0; i< 80; ++i) {
			changeAlpha(1);
			render.renderMap(frames[0]);
			Display.update();
			try {
				Thread.sleep(140);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		render.drawText(0, 0, "", Color.white);
	}
	private void changeAlpha(int step) {
		this.a += step;
		if(a > 255) a = 255;
		if(a < 0) a = 0;
		Color crazyColor = new Color(r,g,b,this.a);
		textRender.drawText(0, 0, "", crazyColor);
	}
}
