 /*  
    Panacea RPG
    Copyright (C) 2019  María Arranz Lobo, Carlos Tardón Rubio, Carlos Morán Alfonso, Marcos Herrero Agustín, Celia Rubio Madrigal, Flavius Abel Ciapsa, 		Martín Fernández de Diego, Ángel Benítez Gómez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package presentacion.input;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Mouse;

public class MouseHandler {
	private static boolean leftDown;
	private static boolean rightDown;
	public static  void init() {
		try {
			Mouse.create();
			 final Cursor emptyCursor = new Cursor(1, 1, 0, 0, 1, BufferUtils.createIntBuffer(1), null);
		        Mouse.setNativeCursor(emptyCursor);
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		leftDown = false;
		rightDown = false;
	}
	public static void processEvents() {
		leftDown = false;
		rightDown = false;
		while(Mouse.next()) {
			switch(Mouse.getEventButton()) {
			case 0:
				if(Mouse.getEventButtonState())leftDown = true;
				break;
			case 1:
				if(Mouse.getEventButtonState())rightDown = true;
				break;
			}
		}
	}
	public static boolean isLeftDown() {
		return leftDown;
	}
	public static boolean isRightDown() {
		return rightDown;
	}
	public static void destroy() {
		Mouse.destroy();
	}
	public static int getX() {
		return Mouse.getX();
	}
	public static int getY() {
		return Mouse.getY();
	}
		
}
